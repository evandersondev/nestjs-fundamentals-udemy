import { Injectable, NotFoundException } from '@nestjs/common'
import { CreateUserDTO } from './dtos/create-user.dto'
import { PrismaService } from 'src/prisma/prisma.service'
import { UpdatePutUserDTO } from './dtos/update-put-user.dto'
import { UpdatePatchUserDTO } from './dtos/update-patch-user.dto'

@Injectable()
export class UserService {
  constructor(private readonly prisma: PrismaService) {}

  async create(data: CreateUserDTO) {
    await this.prisma.user.create({
      data,
    })
  }

  async list() {
    return this.prisma.user.findMany()
  }

  async show(id: number) {
    await this.exists(id)

    return this.prisma.user.findUnique({
      where: { id },
    })
  }

  async update(
    id: number,
    { name, email, password, birthDay }: UpdatePutUserDTO,
  ) {
    await this.exists(id)

    await this.prisma.user.update({
      where: { id },
      data: {
        name,
        email,
        password,
        birthDay: birthDay ? new Date(birthDay) : null,
      },
    })
  }

  async updatePartial(
    id: number,
    { name, email, password, birthDay }: UpdatePatchUserDTO,
  ) {
    await this.exists(id)

    const data: any = {}

    if (birthDay) {
      data.birthDay = new Date(birthDay)
    }

    if (email) {
      data.email = email
    }

    if (name) {
      data.name = name
    }

    if (password) {
      data.password = password
    }

    await this.prisma.user.update({
      where: { id },
      data,
    })
  }

  async delete(id: number) {
    await this.exists(id)

    await this.prisma.user.delete({
      where: { id },
    })
  }

  async exists(id: number) {
    if (!(await this.prisma.user.count({ where: { id } }))) {
      throw new NotFoundException(`The user with id=${id} not exists!`)
    }
  }
}
