import { Body, Controller, Delete, Get, Patch, Post, Put } from '@nestjs/common'
import { CreateUserDTO } from './dtos/create-user.dto'
import { UpdatePutUserDTO } from './dtos/update-put-user.dto'
import { UpdatePatchUserDTO } from './dtos/update-patch-user.dto'
import { UserService } from './user.service'
import { ParamId } from 'src/decorators/param-id.decorator'

// @UseInterceptors(LogInterceptor)
@Controller('users')
export class UserController {
  constructor(private readonly service: UserService) {}

  @Post()
  async create(@Body() { name, email, password }: CreateUserDTO) {
    await this.service.create({ name, email, password })
  }

  @Get()
  async list() {
    return this.service.list()
  }

  @Get(':id')
  async show(@ParamId() id: number) {
    return this.service.show(id)
  }

  @Put(':id')
  async update(@Body() body: UpdatePutUserDTO, @ParamId() id: number) {
    await this.service.update(id, body)
  }

  @Patch(':id')
  async updatePartial(@Body() body: UpdatePatchUserDTO, @ParamId() id: number) {
    await this.service.updatePartial(id, body)
  }

  @Delete(':id')
  async delete(@ParamId() id: number) {
    await this.service.delete(id)
  }
}
